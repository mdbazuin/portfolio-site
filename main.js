var currentActive = "home";
var navLinks = document.getElementsByClassName("nav-link")
var changing = false;

setTimeout(function() { document.getElementById("home").style.opacity = 100}, 50);

document.onkeyup = function (ev) {
  if (ev.key === "ArrowDown" || ev.key === "ArrowRight") {
    try {
      document.getElementById(currentActive).getElementsByClassName("main-footer")[0].getElementsByTagName("span")[0].click();
    } catch (e) {
      return;
    }
  }
  if (ev.key === "ArrowUp" || ev.key === "ArrowLeft") {
    try {
      document.getElementById(currentActive).getElementsByClassName("main-footer")[0].getElementsByClassName("nav-link hidden")[0].click();
    } catch (e) {
      return;
    }
  }
}
window.isScrolling = false;
document.onmousewheel = function(ev) {
  if(window.isScrolling) {
    return
  }
  window.isScrolling = true;
  setTimeout(function() {
    window.isScrolling = false
  }, 900);

  if (ev.deltaY > 0) {
    document.getElementById(currentActive).getElementsByClassName("main-footer")[0].getElementsByTagName("span")[0].click();
  } else if (ev.deltaY < 100) {
    document.getElementById(currentActive).getElementsByClassName("main-footer")[0].getElementsByClassName("nav-link hidden")[0].click();
  }
}

var inputFields = document.getElementsByClassName("input-container");
for (var i = 0; i < inputFields.length; i++) {
  inputFields[i].getElementsByTagName("input")[0].addEventListener("focus", function(ev) { this.setAttribute("placeholder", ""); })
  inputFields[i].getElementsByTagName("input")[0].addEventListener("active", function(ev) { this.setAttribute("placeholder", ""); })
  inputFields[i].getElementsByTagName("input")[0].addEventListener("blur", function(ev) { this.setAttribute("placeholder", this.getAttribute("data-placeholder")); })
}

document.getElementsByClassName("open-button")[0].addEventListener("click", function() {
  document.getElementsByTagName("aside")[0].classList.toggle("open");
});

document.getElementsByClassName("close-button")[0].addEventListener("click", function() {
  document.getElementsByTagName("aside")[0].classList.toggle("open");
});

var navHash = {
  'home': {
    backgroundTransparency: "0.0",
    whiteText: false
  },
  'experience': {
    backgroundTransparency: "0.2",
    whiteText: false
  },
  'education': {
    backgroundTransparency: "0.4",
    whiteText: false
  },
  'skills': {
    backgroundTransparency: "0.6",
    whiteText: true
  },
  'portfolio': {
    backgroundTransparency: "0.8",
    whiteText: true
  },
};


function changePage(identifier) {
  if (currentActive === identifier) {
    return;
  }
  if (changing) {
    return;
  }

  var page = navHash[identifier];
  if (!page) {
    return;
  }
  var newActiveElement = document.getElementById(identifier);
  if (!newActiveElement) {
    return
  }
  changing = true;
  if (identifier === "home") {
    document.getElementById("nav-logo").style.opacity = "0";
  } else {
    document.getElementById("nav-logo").style.opacity = "100";
  }
  document.getElementsByTagName("aside")[0].classList.remove("open");


  setTimeout(function() {
    if (page.whiteText) {
      document.getElementsByTagName("body")[0].classList.add("white")
    } else {
      document.getElementsByTagName("body")[0].classList.remove("white")
    }
  }, 150);

  document.getElementsByTagName("body")[0].style.backgroundColor = "rgba(0,114,255," + page.backgroundTransparency + ")";
  var activeElements = document.getElementsByClassName("active");
  for (var j = 0; j < activeElements.length; j++) {
    activeElements[j].style.opacity = "0";
    var el = activeElements[j];
    setTimeout(function() { el.classList.remove("active");}, 500); 
  }  

  newActiveElement.classList.add("active");
  setTimeout(function() { newActiveElement.style.opacity = "100"}, 500);
  currentActive = identifier;
  window.scrollTo(0,0);
  changing = false;
}

window.addEventListener("hashchange", function() {
  console.log("hi")
  changePage(location.hash.toLocaleLowerCase().replace("#", ""));
});

window.onload = function() {
  if (location.hash !== "") {
    changePage(location.hash.toLocaleLowerCase().replace("#", ""));
  }
}